class UsersController < ApplicationController
    def index 
        @users = User.all 
    end 

    def create 
        @user = User.new(users_params)
        if @user.save 
            redirect_to root_url
        else 
            redirect_to root_url
        end 
    end 

    private 
        def users_params
            params.require("user").permit("name")
        end 
end
